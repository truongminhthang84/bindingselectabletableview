//
//  LoginViewController.swift
//  Selectable
//
//  Created by ThangTM-PC on 11/22/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    let viewModel = LoginViewModel()
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var wrongUsernameLable: UILabel!
    @IBOutlet weak var wrongPasswordLabel: UILabel!
    @IBOutlet weak var heightUsernameConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightPasswordConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        username.bind(with: viewModel.username)
        password.bind(with: viewModel.password)
        viewModel.usernameAlert.addObserver {[unowned self]  (text) in
            
            UIView.animate(withDuration: 0.2) {
                self.heightUsernameConstraint.constant = (text == nil) ? 0 : 20.5
                self.view.layoutIfNeeded()
            }
            self.wrongUsernameLable.text = text
        }
        
        viewModel.passwordAlert.addObserver { [unowned self]  (text) in
            
            UIView.animate(withDuration: 0.2) {
                self.heightPasswordConstraint.constant = (text == nil) ? 0 : 20.5
                self.view.layoutIfNeeded()
            }
            self.wrongPasswordLabel.text = text
        }
        
    }
    
    @IBAction func login(sender: UIButton) {
        viewModel.login().addObserver { [unowned self] (result) in
            switch result {
            case .success(_):
                self.performSegue(withIdentifier: "showDasboard", sender: nil)
            case .failure(let error):
                if error is LoginViewModel.LoginError {
                    let error = error as! LoginViewModel.LoginError
                }
            }
        }
    }
    
}
