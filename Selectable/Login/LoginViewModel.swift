//
//  LoginViewModel.swift
//  Selectable
//
//  Created by ThangTM-PC on 11/22/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import Foundation
import DataBinding

class LoginViewModel {
    
    enum LoginError: String, Error {
        case wrongPassword = "Sai password rồi"
        case wrongUsername = "Sai username rồi"
        case wrongUsernameAndPassword
    }
    
    let username = MutableLiveData<String?>(nil)
    let password = MutableLiveData<String?>(nil)
    
    private var _usernameAlert = MutableLiveData<String?>(nil)
    var usernameAlert: LiveData<String?> {
        return _usernameAlert
    }
    
    private var _passwordAlert = MutableLiveData<String?>(nil)
    var passwordAlert: LiveData<String?> {
        return _passwordAlert
    }
    
    func login() -> LiveData<Result<Void, Error>> {
        var error: LoginError? = nil

        if let username = username.value, username == "Truong Minh Thang"  {
            _usernameAlert.value = nil
        } else  {
            error = LoginError.wrongUsername
            _usernameAlert.value = error?.rawValue
        }
        
        if let password = password.value, password == "1234567"  {
            _passwordAlert.value = nil
        } else {
            if error == nil  {
                 error =  LoginError.wrongPassword
                _passwordAlert.value = error?.rawValue

            } else {
                error = LoginError.wrongUsernameAndPassword
                _passwordAlert.value = "Sai password rồi"
            }
        }
        if error != nil {
            return LiveData<Result<Void, Error>>(Result.failure(error!))
        } else {
            return LiveData<Result<Void, Error>>(Result.success(Void()))
        }
    }
}
