//
//  ViewController.swift
//  Selectable
//
//  Created by ThangTM-PC on 11/19/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var vm = ViewModel()
    @IBOutlet weak var tableView: UITableView!
    var dataSource = TableViewDataSource<UITableViewCell, ItemViewModel>(cellId: "cell") { (cell: UITableViewCell, item: ItemViewModel) in
        cell.textLabel?.text = "\(item.title)"
        cell.accessoryType = item.isSelected ? .checkmark : .none
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.dataSource = dataSource
        dataSource.bind(items: vm.numberViewModels)
    }
    
    @IBAction func addNumber(sender: UIButton) {
        vm.addNumber()
    }
    
    @IBAction func goToInput(sender: UIButton) {
       
    }
    

    
    
    
    
    
}




