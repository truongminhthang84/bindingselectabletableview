//
//  ItemViewModel.swift
//  Selectable
//
//  Created by ThangTM-PC on 11/20/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import Foundation

struct ItemViewModel: Selectable, Equatable {
    var title: String
    var isSelected: Bool = false
   
    
}

public protocol Selectable {
    var isSelected: Bool {get set}

}

// MARK: - <#Mark#>

extension Selectable {
    public mutating func setSelected(_ selected: Bool) {
        isSelected = selected
    }
    
    public mutating func toggle() {
        isSelected = !isSelected
    }
}
