//
//  ViewModel.swift
//  Selectable
//
//  Created by ThangTM-PC on 11/19/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import Foundation
import DataBinding

class ViewModel {
    enum Exception {
        case somethingWrong
    }
    
    var numberViewModels = MutableLiveData<[ItemViewModel]>(
        [Int](0...20).map({ (number) in
            var  item = ItemViewModel(title: "\(number)")
            if number % 2 == 0 {
                item.isSelected = true
            }
            return item
        })
    )
    func addNumber() {
        numberViewModels.value.append(ItemViewModel(title: "1000"))
    }
    
}






