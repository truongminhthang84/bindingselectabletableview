//
//  TableViewDataSource.swift
//  Selectable
//
//  Created by ThangTM-PC on 11/19/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import UIKit
import DataBinding

public class TableViewDataSource<CellType: UITableViewCell, ViewModel: Selectable & Equatable  >: NSObject, UITableViewDataSource, UITableViewDelegate {
        
    let cellId: String
    public var itemsLiveData : MutableLiveData<[ViewModel]>?
    var items: [ViewModel] = []{
        didSet {
            tableView?.reloadData()
            if let value = itemsLiveData?.value, items != value {
                    itemsLiveData?.value = items
            }
        }
    }
    let configure: (CellType, ViewModel) -> ()
    
    weak var tableView: UITableView? {
        didSet {
            tableView?.delegate = self
        }
    }
    
    init(cellId: String, configure: @escaping (CellType, ViewModel) -> Void) {
        self.cellId = cellId
        self.configure = configure
    }
    
    func insertItem(_ item: ViewModel, at index: Int) {
        self.items.insert(item, at: index)
        itemsLiveData?.value = self.items
        if items.count == 1 {
            tableView?.reloadData()
        } else {
            tableView?.insertRows(at: [IndexPath(row: items.count - 1, section: 0)], with: .automatic)
        }
    }
        
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.tableView == nil {
            self.tableView = tableView
        }
        return items.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CellType
        let vm = items[indexPath.row]
        configure(cell, vm)
        return cell
    }
    
    func bind(items liveData: MutableLiveData<[ViewModel]>) {
        itemsLiveData = liveData
        itemsLiveData?.addObserver { items in
            self.items = items
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        items[indexPath.row].toggle()
    }
    
    public func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        items[indexPath.row].setSelected(false)
    }

    
    
    
    
    
}

