//
//  InputViewModel.swift
//  Selectable
//
//  Created by ThangTM-PC on 11/20/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import Foundation
import DataBinding

class InputViewModel {
    
    
    private var _labelText = MutableLiveData<String?>("")
    var labelText: LiveData<String?> {
        return _labelText
    }

    var textFieldText = MutableLiveData<String?>("")

    func fillText() {

        _labelText.value = (labelText.value ?? "") + "1"
        textFieldText.value = (textFieldText.value ?? "") + "1"
    }
    
}
