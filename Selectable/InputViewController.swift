//
//  InputViewController.swift
//  Selectable
//
//  Created by ThangTM-PC on 11/20/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import UIKit
import DataBinding

class InputViewController: UIViewController {
    let vm = InputViewModel()
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        label.bind(to: vm.labelText)
        textField.bind(with: vm.textFieldText)
        
    }
    
    @IBAction func fillData(sender: UIButton) {
        vm.fillText()
    }
}

//public class LiveData<ObservedType> {
//    public typealias Observer = (ObservedType) -> Void
//
//    private var listeners: [Observer] = []
//
//    public var value: ObservedType {
//        didSet {
//               self.listeners.forEach { (callback) in
//                    callback(value)
//                }
//        }
//    }
//
//    public init(_ value: ObservedType) {
//        self.value = value
//    }
//
//    public func addObserver(observer: @escaping Observer) {
//        self.listeners.append(observer)
//        observer(value)
//    }
//}
//
//
//
//
//extension UILabel{
//    public func bind(to livedata: LiveData<String?>) {
//        livedata.addObserver { text in
//            self.text = text
//        }
//    }
//}
