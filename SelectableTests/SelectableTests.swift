//
//  SelectableTests.swift
//  SelectableTests
//
//  Created by ThangTM-PC on 11/19/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import XCTest
import DataBinding

class SelectableTests: XCTestCase {
    let viewModel = LoginViewModel()
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLoginSuccess() {
        
        viewModel.password.value = "1234567"
        viewModel.username.value = "Truong Minh Thang"
        viewModel.login().addObserver { (result) in
            switch result {
            case .success:
                XCTAssert(true)
            case .failure:
                XCTAssert(false)
            }
        }
        XCTAssertNil(viewModel.passwordAlert.value )
        XCTAssertNil(viewModel.usernameAlert.value )
    }
    
    func testLoginFail() {
        viewModel.password.value = "12345"
        viewModel.username.value = "Truong Minh"
        viewModel.login().addObserver { (result) in
            switch result {
            case .success:
                XCTAssert(false)
            case .failure:
                XCTAssert(true)
            }
        }
        XCTAssertEqual(viewModel.passwordAlert.value, "Sai password rồi" )
        XCTAssertEqual(viewModel.usernameAlert.value , "Sai username rồi")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
           testLoginFail()
        }
    }

}
